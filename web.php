<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('user',                CORE.'User\UserController',                      ['except' => ['create', 'edit']]);

$logicasPath = app_path().DIRECTORY_SEPARATOR."Domain";
$logicasPathContent = scandir($logicasPath);
foreach ($logicasPathContent as $item) {
    if ($item != "." && $item != "..") {
        Route::resource($item, "App\\Domain\\".$item."\\".$item."Controller", ['except' => ['create', 'edit']]);
    }
}