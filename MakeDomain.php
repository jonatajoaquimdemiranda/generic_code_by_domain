<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use App\CustomCommandsUtils\MakeDomain\ControllerGenerator;
use App\CustomCommandsUtils\MakeDomain\ModelGenerator;
use App\CustomCommandsUtils\MakeDomain\RepositoryEloquentGenerator;
use App\CustomCommandsUtils\MakeDomain\RepositoryInterfaceGenerator;
use App\CustomCommandsUtils\MakeDomain\ServiceGenerator;
use Prettus\Repository\Generators\FileAlreadyExistsException;

class MakeDomain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:domain {name} {--fillable=value} {--table=value}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria Domain';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->generators = new Collection();

        $modelGenerator = new ModelGenerator([
            'name'     => $this->argument('name'),
            'fillable' => $this->option('fillable'),
            'table' => $this->option('table')
        ]);

        $this->generators->push($modelGenerator);

        $controllerGenerator = new ControllerGenerator([
            'name'     => $this->argument('name')
        ]);

        $this->generators->push($controllerGenerator);

        $serviceGenerator = new ServiceGenerator([
            'name'     => $this->argument('name')
        ]);

        $this->generators->push($serviceGenerator);

        $this->generators->push(new RepositoryInterfaceGenerator([
            'name'  => $this->argument('name')
        ]));

        foreach ($this->generators as $generator) {
            $generator->run();
        }

        $model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();
        $model = str_replace([
            "\\",
            '/'
        ], '\\', $model);

        try {
            (new RepositoryEloquentGenerator([
                'name'      => $this->argument('name'),
                'model'     => $model
            ]))->run();
            $this->info("Repository created successfully.");
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }
}
