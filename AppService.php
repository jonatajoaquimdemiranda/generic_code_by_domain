<?php
namespace App\Util;

use Faker\Factory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\RepositoryInterface;

class AppService
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var Validator
     */
    protected $validator;

    protected $rules = [];
    protected $rulesUpdate = [];

    public function show($id)
    {
        return $this->repository
            ->with($this->repository->makeModel()->parents())
            ->find($id);
    }

    public function index() {
        return $this->repository->all();
    }

    public static function getNextSequencial($table, $field) {
        $nextSequencial = DB::table($table)->selectRaw("(COALESCE(MAX($field), 0) + 1) AS next")->get();
        return $nextSequencial;
    }

    public function getRandomField($classNameService, $field) {
        $service = App::make($classNameService);
        $values = $service->index();
        $array = [];
        foreach ($values as $item) {
            array_push($array, $item->$field);
        }
        if (count($array) < 1) return null;
        $randomValue = array_rand($array);
        return $array[$randomValue];
    }

    public function getFakerFillable($classNameModel) {
        $faker = Factory::create();
        $model = new $classNameModel();
        $arrayToReturn = [];
        foreach ($model->fillablePublic as $key => $value) {
            if (strpos($model->fillableType[$key],'sequencial') !== false) {
                $valuesSequencial = explode(":",$model->fillableType[$key]);

                $nextSequencial = self::getNextSequencial($model->table, $valuesSequencial[1]);
                $arrayToReturn[$value] = $nextSequencial[0]->next;
            }
            if (strpos($model->fillableType[$key],'foreign') !== false) {
                $valuesForeign = explode(":",$model->fillableType[$key]);
                $serviceForeignPath = "\\App\\Domain\\".$valuesForeign[1]."\\".$valuesForeign[1]."Service";
                $valueRamdom = $this->getRandomField($serviceForeignPath, $valuesForeign[2]);

                if ($valueRamdom == null) throw new Exception("Não foi encontrado valor no model ".$valuesForeign[1]);

                $arrayToReturn[$value] = $valueRamdom;
            }
            if ($model->fillableType[$key] == 'name') {
                $arrayToReturn[$value] = $faker->name;
            }
            if ($model->fillableType[$key] == 'cnpj') {
                $arrayToReturn[$value] = "999999999999999";
            }
        }
        return $arrayToReturn;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(array $data)
    {
        if (isset($this->validator)) {
            $this->validator->setRules($this->rules);
            $this->validator->with($data)->passesOrFail();
        }
        try {
            DB::beginTransaction();
            $entity = $this->repository->create($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $this->repository->find($entity->id);
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(array $data, $id)
    {
        if (isset($this->validator)) {
            $this->validator->setRules($this->rulesUpdate);
            $this->validator->with($data)->passesOrFail();
        }
        try {
            DB::beginTransaction();
            $entity = $this->repository->find($data['id'] == $id ? $id : 0);
            $entity->update($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $this->show($entity->id);
    }

    public function delete($id) {
        $this->repository->delete($id);
    }
}