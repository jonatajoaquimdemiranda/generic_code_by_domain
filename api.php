<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$logicasPath = app_path().DIRECTORY_SEPARATOR."Domain";
$logicasPathContent = scandir($logicasPath);
foreach ($logicasPathContent as $item) {
    if ($item != "." && $item != "..") {
        Route::resource($item, "App\\Domain\\".$item."\\".$item."Controller", ['except' => ['create', 'edit']]);
    }
}
