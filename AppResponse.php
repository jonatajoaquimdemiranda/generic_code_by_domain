<?php

namespace App\Util;

class AppResponse
{
    public function Response($data = null, $message = '',$status = 0, $httpStatus = 200){


        return response()
            ->json(['status' => $status, 'data' => $data, 'message' => $message], $httpStatus)
            ->header('Access-Control-Allow-Origin','*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With')
            ->header('Access-Control-Allow-Credentials', 'true');
    }

}